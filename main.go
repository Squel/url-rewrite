package main

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
)

var text string

func main() {
	app := &cli.App{
		Name:  "url-rewrite",
		Usage: "rewrite urls into a \"cleaner\" format",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "url",
				Aliases: []string{"i", "u"},
				Value:   "https://example.com",
				Usage:   "url to parse",
			},
			&cli.BoolFlag{
				Name:    "frontend",
				Aliases: []string{"f"},
				Usage:   "rewrite to a front-end, instead of an short url",
			},
		},
		Action: func(cCtx *cli.Context) error {
			if cCtx.String("url") == "https://example.com" {
				reader := bufio.NewReader(os.Stdin)
				fmt.Print("Enter url: ")
				text, _ = reader.ReadString('\n')
			} else {
				text = (cCtx.String("url"))
			}
			fmt.Println(UrlRewrite(text, cCtx.Bool("frontend")))
			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
