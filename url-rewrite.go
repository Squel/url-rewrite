package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"regexp"
	"strings"
)

type rewriteRule struct {
	DomainDetection   string                    `json:"domainDetection"`
	ExtractInfo       map[string]*regexp.Regexp `json:"extractInfo"`
	RewriteFormat     string                    `json:"rewriteFormat"`
	RewriteFormatFOSS string                    `json:"rewriteFormatFOSS"`
}

// Map domain to rewrite rule.
var urlRewrites = map[string]rewriteRule{}

func init() {
	file, err := os.Open("rewrite-rules.json")
	if err != nil {
		// Error opening JSON file: %v", err
		panic(fmt.Sprintf("Error opening JSON file: %v", err))
	}
	defer file.Close()

	err = json.NewDecoder(file).Decode(&urlRewrites)
	if err != nil {
		panic(fmt.Sprintf("Error decoding JSON: %v", err))
	}
}

func UrlRewrite(inputurl string, foss bool) (string, error) {
	// Iterate over all the rewrite rules.
	for _, rewriteRule := range urlRewrites {
		if !strings.Contains(inputurl, rewriteRule.DomainDetection) {
			// Skipped %s: %s does not contain host/domain %s", service, inputurl, rewriteRule.DomainDetection
			continue
		}

		rewriteResult := rewriteRule.RewriteFormat
		if foss {
			rewriteResult = rewriteRule.RewriteFormatFOSS
		}

		// Iterate over the key and regexs in 'extractInfo'.
		matchCount := false
		for key, regex := range rewriteRule.ExtractInfo {
			// Find a match in the URL.
			matches := regex.FindStringSubmatch(inputurl)

			// Replace the value with nothing if there isn't a match, or
			// with the latest match of the regexp rule.
			// save that there has been a match
			// `replaceValue` will stay an empty string otherwise `""`
			replaceValue := ""
			if matches != nil {
				matchCount = true // default == false
				replaceValue = matches[len(matches)-1]
			}

			rewriteResult = strings.ReplaceAll(rewriteResult, "{"+key+"}", replaceValue)
		}

		if !matchCount {
			return "", errors.New("no information could be extracted")
		}

		return "https://" + rewriteResult, nil
	}
	// No rewrite rules match
	return "", errors.New("hostname did not match any supported service")
}
