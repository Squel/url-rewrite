// go run . (-v)

package main

import (
	"testing"
)

type testCase struct {
	name        string // service_extractinfo_description_error_frontend
	input       string
	output      string
	frontend    bool
	shouldError bool
}

func TestRewrite(t *testing.T) {
	var testCases = []testCase{
		{
			// This service isn't supported by the url-rewrite project and it
			// should fail at all times.
			name:        "global_error_unsupported",
			input:       "https://example.org",
			output:      "",
			frontend:    false,
			shouldError: true,
		},
		{
			name:        "youtube-com_video-id_all-ids",
			input:       "https://youtube.com/watch?v=lorem-_",
			output:      "https://youtu.be/lorem-_",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_timestamp",
			input:       "https://youtube.com/watch?v=lorem&t=10",
			output:      "https://youtu.be/lorem&t=10",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_timestamp_seconds",
			input:       "https://youtube.com/watch?v=lorem&t=10s",
			output:      "https://youtu.be/lorem&t=10s",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_timestamp_minutes",
			input:       "https://youtube.com/watch?v=lorem&t=10m",
			output:      "https://youtu.be/lorem&t=10m",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_timestamp_hours",
			input:       "https://youtube.com/watch?v=lorem&t=10h",
			output:      "https://youtu.be/lorem&t=10h",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_timestamps",
			input:       "https://youtube.com/watch?v=lorem&t=10h10m10s",
			output:      "https://youtu.be/lorem&t=10h10m10s",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_simple_frontend",
			input:       "https://youtube.com/watch?v=lorem",
			output:      "https://redirect.invidious.io/watch?v=lorem",
			frontend:    true,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_all-ids_frontend",
			input:       "https://youtube.com/watch?v=lorem-_",
			output:      "https://redirect.invidious.io/watch?v=lorem-_",
			frontend:    true,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_timestamp_simple_frontend",
			input:       "https://youtube.com/watch?v=lorem&t=10",
			output:      "https://redirect.invidious.io/watch?v=lorem&t=10",
			frontend:    true,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_timestamp_seconds_frontend",
			input:       "https://youtube.com/watch?v=lorem&t=10s",
			output:      "https://redirect.invidious.io/watch?v=lorem&t=10s",
			frontend:    true,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_timestamp_minutes_frontend",
			input:       "https://youtube.com/watch?v=lorem&t=10m",
			output:      "https://redirect.invidious.io/watch?v=lorem&t=10m",
			frontend:    true,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_timestamp_hours_frontend",
			input:       "https://youtube.com/watch?v=lorem&t=10h",
			output:      "https://redirect.invidious.io/watch?v=lorem&t=10h",
			frontend:    true,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id+timestamp_complex-combination_frontend",
			input:       "https://youtube.com/watch?v=lorem&t=10h10m10s",
			output:      "https://redirect.invidious.io/watch?v=lorem&t=10h10m10s",
			frontend:    true,
			shouldError: false,
		},
		{
			name:        "youtube-com_video-id_no-id_error",
			input:       "https://youtube.com/watch",
			output:      "",
			frontend:    false,
			shouldError: true,
		},
		{
			name:        "youtube-com_error_no-id",
			input:       "https://youtube.com/watch?v=",
			output:      "",
			frontend:    false,
			shouldError: true,
		},
		{
			name:        "nos-nl_article-id_simple",
			input:       "https://nos.nl/artikel/00000-lorem",
			output:      "https://nos.nl/l/00000",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "nos-nl_article-id_livestream",
			input:       "https://nos.nl/livestream/00000-lorem",
			output:      "https://nos.nl/l/00000",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "nos-nl_article-id_video",
			input:       "https://nos.nl/video/00000-lorem",
			output:      "https://nos.nl/l/00000",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "nos-nl_article-id_liveblog",
			input:       "https://nos.nl/liveblog/00000-lorem",
			output:      "https://nos.nl/l/00000",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "nos-nl_article-id_collection",
			input:       "https://nos.nl/collectie/00000-lorem",
			output:      "",
			frontend:    false,
			shouldError: true,
		},
		{
			name:        "nos-nl_article-id_collection-with-article",
			input:       "https://nos.nl/collectie/00000/artikel/11111-lorem",
			output:      "https://nos.nl/l/11111",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "nos-nl_article-id_no-id_error",
			input:       "https://nos.nl/artikel/",
			output:      "",
			frontend:    false,
			shouldError: true,
		},
		{
			name:        "nos-nl_article-id_no-article_error",
			input:       "https://nos.nl/",
			output:      "",
			frontend:    false,
			shouldError: true,
		},
		{
			name:        "reddit-com_post-id_simple",
			input:       "https://www.reddit.com/r/placeholder/comments/lorem/placeholder/",
			output:      "https://redd.it/lorem",
			frontend:    false,
			shouldError: false,
		},
		{
			name:        "reddit-com_post-id_oldreddit",
			input:       "https://old.reddit.com/r/placeholder/comments/lorem/placeholder/",
			output:      "https://redd.it/lorem",
			frontend:    false,
			shouldError: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			result, err := UrlRewrite(testCase.input, testCase.frontend)
			if testCase.shouldError && err == nil {
				t.Fatalf("expected error: %v, got: %v", testCase.shouldError, err)
			}
			if result != testCase.output {
				t.Fatalf("expected: %v, got: %v", testCase.output, result)
			}
		})
	}
}
