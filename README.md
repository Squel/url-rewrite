# url-rewrite

A project inspired by [LibRedirect](https://github.com/libredirect/browser_extension) and [xkcd 2235 (rule 2)](https://xkcd.com/2235/) to rewrite urls to redirect to a privacy-respecting front-end or remove tracking (or any other unnecessary) parameters, with a focus on easy extensibility.

## Scope of the project

This repository will contain a CLI-version of the url-rewrite tool, written in Go, which was forced upon me to be used. The project is also [integrated](https://git.gusted.xyz/S.B.I./sbi-bot/pulls/9) into a Discord bot, for the ones that do not follow rule #2.

The url will only be rewritten from the information (domains, paths, parameters, etc.) provided by the input url, there is no functionality planned to add API calls to resolve to a target page.

## General configuration

Below is an example configuration. It is a [JSON file](./rewrite-rules.json) that gets parsed and consist of the following parts:
- name of the service
- a domain detection (`"domainDetection"`), which will be used to detect what service rules to use
- a user-defined name for parameter and regex string, this information is to be extracted `"extractInfo" > "id-ABC"`
- two rewrite formats with placeholders for parameters. Note that the placeholder name is the KEY of the 'extractInfo' parameter. `"rewriteFormat"`
```json
{
    "name-of-service": {
        "domainDetection": "example.com",
        "extractInfo": {
            "id-ABC": "REGEX-HERE"
        },
        "rewriteFormat": "example.com/{id-ABC}",
        "rewriteFormatFOSS": "example.org/{id-ABC}"
    }
}
```


## CLI
### General usage

```bash
go run . [global options]
# or
go build . && ./url-rewrite [global options]
```

```bash
$ ./url-rewrite --url https://www.youtube.com/watch\?v\=dQw4w9WgXcQ
#>> https://www.youtube.com/watch\?v\=dQw4w9WgXcQ

$ ./url-rewrite --url https://www.youtube.com/watch\?v\=dQw4w9WgXcQ\&t\=101 --frontend
#>> https://invidious.projectsegfau.lt/watch?v=dQw4w9WgXcQ&t=101

$ ./url-rewrite
Enter url: https://www.youtube.com/watch\?v\=dQw4w9WgXcQ
#>> https://youtu.be/dQw4w9WgXcQ
```


### Help page

```
NAME:
   url-rewrite - rewrite urls into a "cleaner" format

USAGE:
   url-rewrite [global options] command [command options] 

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --url value, -i value, -u value  url to parse (default: "https://example.com")
   --frontend, -f                   rewrite to a front-end, instead of an short url (default: false)
   --help, -h                       show help
```
(can be found by running the application with the `-h` flag)

## License
This project, including the rules are MIT licensed. More information can be found in the [`LICENSE` file](./LICENSE).
